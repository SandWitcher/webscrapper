*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           ExcelLibrary
Library           Collections
Library           String

*** Variables ***
${START_URL}      https://trajectoire.sante-ra.fr/Trajectoire/pages/AccesLibre/Annuaires/Etablissement.aspx
${TABLE_URL}      file:///C:/Users/hugob/PycharmProjects/WebScraper/demo/TestPage.html
${LIST_URL}       file:///C:/Users/hugob/PycharmProjects/WebScraper/demo/ListTest.html
${BROWSER}        Chrome
${DELAY}          0
${TIMEOUT}        25


#FIRST PAGE
${COMPLEX_SEARCH_TOGGLE}             xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe"]/ul/li/a
${PATHOLOGIES_DROPDOWN}              xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe_i0_rcbPathologies_Arrow"]
${PATHOLOGIES_ADULTS_TOGGLE}         xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe_i0_rcbPathologies_i0_rtvPathologies"]/ul/li[1]/div/span[2]
${PATHOLOGIES_RHUMATOLOGIES_TEXT}    xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe_i0_rcbPathologies_i0_rtvPathologies"]/ul/li[1]/ul/li[18]/div/span[2]
${PATHOLOGIES_RHUMATOLOGIES_TOGGLE}  xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe_i0_rcbPathologies_i0_rtvPathologies"]/ul/li[1]/ul/li[18]/ul/li[3]/div/span[3]
${SUBMIT_SEARCH_BUTTON}              xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe_i0_btRechercheComplexe"]


#REGION DROPDOWN
${REGION_DROPDOWN}                   xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe_i0_rcbTerritoires_Arrow"]
${REGION_TABLE_UL_LI}                   xpath=//div[@id="ctl00_ctl00_GlobalContent_MainContent_rpbRechCplxe_i0_rcbTerritoires_i0_rtvTerritoires"]/ul[@class="rtUL rtLines"]/li


#LIST PAGE
${ITEMS}                            xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_upListViewStructures"]/div[1]/div[@class="VignetteStructure"]
${NEXT_BUTTON}                      xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rdpStructures_ctl02_NextButton"]
${ITEMS_PER_PAGE_DROPDOWN}          xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rdpStructures_ctl04_PageSizeComboBox_Arrow"]
${54_ITEMS_PER_PAGE}                xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_rdpStructures_ctl04_PageSizeComboBox_DropDown"]/div/ul/li[4]

${HOSPITAL_FORM}                    xpath=//div[@id="FicheS"]
${UNIT_FORM}                        xpath=//div[@id="FicheU"]



#EXCEL
${EXCEL_DOCUMENT}
${EXCEL_FILENAME}

${HOSPITAL_SHEET}     Hospitals
${UNIT_SHEET}         Units
${STAFF_SHEET}        Staff

${HOSPITAL_ROW}       ${2}
${UNIT_ROW}           ${2}
${STAFF_ROW}          ${2}

#HOSPITAL FORM
${UNITS_TAB}                   xpath=//*[@id="BlocOngletFicheS"]/ul/li[2]
${UNITS_TABLE_BODY}            //div[@id="pUnites"]/table/tbody
${CLOSE_HOSPITAL_FORM}         xpath=//*[@id="pFicheStructurePopup"]/div[1]/span


#---INFORMATION---
${HOSPITAL_NAME}      xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_lbNom"]
${HOSPITAL_ADDRESS}   xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_lbAdresse"]
${HOSPITAL_PHONE}     xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_lbTel"]
${HOSPITAL_MAIL}      xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_hlEmail"]
${HOSPITAL_FINESS}    xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_lbFiness"]
${HOSPITAL_SIRET}     xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_lbSIRET"]
${HOSPITAL_HEALTH_TERRITORY}  xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_lbTerritoire"]
${HOSPITAL_REF_COORDINATION}  xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheStructurePopup_lbCoordination"]



#UNIT FORM
${UNIT_DISCIPLINE}    xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheUnitePopup_lbDiscipline"]
${UNIT_NAME}          xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheUnitePopup_lbNom"]
${UNIT_BEDS}          xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheUnitePopup_lbLitsInstalles"]
${UNIT_CHIEF}         xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheUnitePopup_lResponsableNomPrenom"]
${UNIT_CHIEF_MAIL}    xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheUnitePopup_lResponsableEmail"]
${UNIT_HOSPITAL}      xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheUnitePopup_lkbNomEtablissementPopup"]

${CURRENT_UNIT_NAME}
${CURRENT_HOSPITAL_NAME}

#---STAFF TAB
${STAFF_TAB}          xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_FicheUnitePopup_OngletHeaderPersonnelFicheU"]
${STAFF_TABLE_BODY}   //div[@id="pPersonnel"]/table/tbody

#STAFF FORM
${PERSON_NAME}        xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_lbNom"]
${PERSON_MAIL}        xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_lb_email"]/a
${PERSON_PHONE}       xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_lb_tel"]
${PERSON_PROFESSION}  xpath=//*[@id="ctl00_ctl00_GlobalContent_MainContent_lbProf"]

${EMPTY_SLOT}         NA

*** Keywords ***
#FIRST PAGE
Open Browser To Main Page
    Open Browser   ${START_URL}  ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Set Selenium Timeout  ${TIMEOUT}

Open Complex Research
    Click Element  ${COMPLEX_SEARCH_TOGGLE}

# Not functional
Select All Regions
    Click Element On Visible  ${REGION_DROPDOWN}
    ${size}  Get Element Count  ${REGION_TABLE_UL_LI}
    ${size}  Set Variable  ${size + 1}

    FOR   ${index}   IN RANGE   1   ${size}
        ${click_element}  Catenate  ${REGION_TABLE_UL_LI}  [${index}]/span[@class="rtUnchecked"]
        Click Element  ${click_element}
    END

Set Pathologies
    Click Element On Visible  ${PATHOLOGIES_DROPDOWN}
    Click Element On Visible  ${PATHOLOGIES_ADULTS_TOGGLE}
    Click Element On Visible  ${PATHOLOGIES_RHUMATOLOGIES_TEXT}
    Click Element On Visible  ${PATHOLOGIES_RHUMATOLOGIES_TOGGLE}
    Click Element  ${PATHOLOGIES_DROPDOWN}

Submit Complex Research
    Click Element On Visible  ${SUBMIT_SEARCH_BUTTON}

#LIST PAGE
Set Items Per Page
    Click Element On Visible  ${ITEMS_PER_PAGE_DROPDOWN}
    Click Element On Visible  ${54_ITEMS_PER_PAGE}
    Wait Until DivMasaquage Is Not Visible

For Each Page
    Wait Until Element Is Visible  ${ITEMS}
    For Each Item
    FOR   ${index}   IN RANGE   1   150
        Click Element  ${NEXT_BUTTON}
        Wait Until DivMasaquage Is Not Visible
        Wait Until Element Is Visible  ${ITEMS}
        For Each Item
    END

For Each Item
    FOR   ${index}   IN RANGE  1  55
        ${item}  Catenate  ${ITEMS}  [${index}]
        Wait Until Element Is Visible  ${item}
        Click Element  ${item}
        Wait Until Element Is Visible  ${HOSPITAL_FORM}

        ${hospitalname}  Write Hospital Row

        Click Element  ${UNITS_TAB}

        Check Units

        Click Element  ${CLOSE_HOSPITAL_FORM}
        Wait Until Element Is Not Visible  ${HOSPITAL_FORM}
    END

Check Units
    ${colspanxpath}  Catenate  ${UNITS_TABLE_BODY}  /tr[2]/td
    ${tableexists}  Get Element Count  ${colspanxpath}
    Run Keyword And Return If  ${tableexists} > 0  For Each Unit

For Each Unit
    ${xpath}  Catenate  ${UNITS_TABLE_BODY}  //tr
    ${size}  Get Element Count  ${xpath}
    ${size}  Set Variable  ${size + 1}

    FOR  ${index}  IN RANGE  3  ${size}
        ${query}  Catenate  ${UNITS_TABLE_BODY}  /tr[${index}]/td[2]/a
        ${count}  Get Element Count  ${query}
        Run Keyword If  ${count} > 0  Get Unit Information  ${query}
    END

Get Unit Information
    [Arguments]  ${xpath}
    Click Element  ${xpath}
    Wait Until Element Is Visible  ${UNIT_FORM}
    Write Unit Row
    Click Element  ${STAFF_TAB}
    Check Staff
    Click Element  ${UNIT_HOSPITAL}
    Wait Until DivMasaquage Is Not Visible
    Wait Until Element Is Visible  ${HOSPITAL_FORM}
    Click Element  ${UNITS_TAB}

Check Staff
    ${tableexists}  Get Element Count  ${STAFF_TABLE_BODY}
    Run Keyword And Return If  ${tableexists} > 0  For Each Person

For Each Person
    ${xpath}  Catenate  ${STAFF_TABLE_BODY}  //tr
    ${size}  Get Element Count  ${xpath}
    ${size}  Set Variable  ${size + 1}

    FOR  ${index}  IN RANGE  2  ${size}
        ${query}  Catenate  ${STAFF_TABLE_BODY}  /tr[${index}]/td[1]/a
        ${count}  Get Element Count  ${query}
        Run Keyword If  ${count} > 0  Get Person Information  ${query}
    END

Get Person Information
    [Arguments]  ${xpath}
    Click Element  ${xpath}
    Select Window  NEW
    Wait Until Element Is Visible  ${PERSON_NAME}
    Write Staff Row
    Close Window
    Select Window  MAIN

Open Excel
    ${EXCEL_DOCUMENT}  Open Excel Document  hospitals.xlsx  docid


Write Hospital Row
    ${name}       Get Information  ${HOSPITAL_NAME}
    ${phone}      Get Information  ${HOSPITAL_PHONE}
    ${mail}       Get Information  ${HOSPITAL_MAIL}
    ${finess}     Get Information  ${HOSPITAL_FINESS}
    ${siret}      Get Information  ${HOSPITAL_SIRET}
    ${coord}      Get Information  ${HOSPITAL_REF_COORDINATION}
    ${territory}  Get Information  ${HOSPITAL_HEALTH_TERRITORY}


    ${full_address}    Get Information  ${HOSPITAL_ADDRESS}

    @{split_address}  split string from right  ${full_address}  ,${SPACE}  1
    ${length}  Get Length  ${split_address}

    ${street}  get from list  ${split_address}  0
    ${city}  run keyword if  ${length} > 1  get from list  ${split_address}  1

    @{split_city}  run keyword if  ${length} > 1  split string  ${city}  ${SPACE}  1
    ${city_code}   run keyword if  ${length} > 1  get from list  ${split_city}  0
    ${city_name}   run keyword if  ${length} > 1  get from list  ${split_city}  1

    ${list}  Create List  ${name}  ${full_address}  ${street}  ${city_code}  ${city_name}  ${territory}  ${coord}  ${phone}  ${mail}  ${finess}  ${siret}
    Write Excel Row  ${HOSPITAL_ROW}  ${list}  0  ${HOSPITAL_SHEET}
    Save Excel

    Set Global Variable  ${HOSPITAL_ROW}  ${HOSPITAL_ROW+1}
    Set Global Variable  ${CURRENT_HOSPITAL_NAME}  ${name}

Get City
    [Arguments]

Write Unit Row
    ${hospital}    Get Information  ${UNIT_HOSPITAL}
    ${name}        Get Information  ${UNIT_NAME}
    ${beds}        Get Information  ${UNIT_BEDS}
    ${chief}       Get Information  ${UNIT_CHIEF}
    ${chiefmail}   Get Information  ${UNIT_CHIEF_MAIL}
    ${discipline}  Get Information  ${UNIT_DISCIPLINE}

    ${list}  Create List  ${hospital}  ${discipline}  ${name}  ${beds}  ${chief}  ${chiefmail}
    Write Excel Row  ${UNIT_ROW}  ${list}  0  ${UNIT_SHEET}
    Save Excel

    Set Global Variable  ${UNIT_ROW}  ${UNIT_ROW+1}
    Set Global Variable  ${CURRENT_UNIT_NAME}  ${name}

Write Staff Row
    ${name}        Get Information  ${PERSON_NAME}
    ${mail}        Get Information  ${PERSON_MAIL}
    ${profession}  Get Information  ${PERSON_PROFESSION}
    ${phone}       Get Information  ${PERSON_PHONE}

    ${list}  Create List  ${CURRENT_HOSPITAL_NAME}  ${CURRENT_UNIT_NAME}  ${name}  ${profession}  ${mail}  ${phone}
    Write Excel Row  ${STAFF_ROW}  ${list}  0  ${STAFF_SHEET}
    Save Excel

    Set Global Variable  ${STAFF_ROW}  ${STAFF_ROW+1}

Save Excel
    Save Excel Document  hospitals.xlsx

Get Information
    [Arguments]  ${xpath}
    ${var}    Set Variable  ${EMPTY_SLOT}
    ${count}  Get Element Count  ${xpath}
    ${var}    Run Keyword And Return If  ${count} > 0   Get Text  ${xpath}
    [Return]  ${var}

Click Element On Visible
    [Arguments]  ${xpath}
    Wait Until Element Is Visible  ${xpath}
    Click Element  ${xpath}

Click Element If Existant
    [Arguments]  ${xpath}
    ${count}  Get Element Count  ${xpath}
    Run Keyword And Return If  ${count} > 0  Click Element  ${xpath}

Wait Until DivMasaquage Is Not Visible
    Wait Until Element Is Not Visible  xpath=//div[@id="DivMasquage"]
