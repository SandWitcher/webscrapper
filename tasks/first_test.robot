*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

*** Test Cases ***
Complete Fields
    Open Browser To Main Page
    Open Complex Research
    Set Pathologies
    Submit Complex Research

Extract Data
    Open Excel
    Set Items Per Page
    For Each Page
    Save Excel
